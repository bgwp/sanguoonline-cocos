
cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad: function() {

        // 点击将领池
        this.node.getChildByName("nodeDown").getChildByName("btnGenerals").on("click", () => {

            cc.loader.loadRes("Prefabs/prefabGenerals", cc.Prefab, (err, prefab) => {

                if (err) {
                    console.log(err);
                    return;
                }

                var generalLayer = cc.instantiate(prefab);
                this.node.addChild(generalLayer);
            });
        });
    },
});
